import React from 'react'
import DashGrid from './shared/DashGrid'
import OverviewChart from './shared/OverviewChart'
import CustomersChart from './shared/CustomersChart'
import ProductSell from './shared/ProductSell'

export default function Dashboard() {
  return (
    <div className='flex flex-col gap-4 p-4 mx-4'>
      <DashGrid/>
      <div className='flex flex-row w-full gap-4 mx-2 my-2'>
      <OverviewChart/>
      <CustomersChart/>
      {/* <DemoChart/> */}
      </div>
      <ProductSell/>
    </div>
  )
}
