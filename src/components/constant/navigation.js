import { HiMiniKey, HiMiniCube, HiMiniUserCircle, HiChatBubbleLeftEllipsis, HiMiniScale, HiReceiptPercent, HiChevronRight } from "react-icons/hi2";

export const SIDEBARTOP_LINK = [
  {
    key:'dashboard',
    label:'DashBoard',
    path:'/',
    icon:<HiMiniKey/>
  },
  {
    key:'products',
    label:'Products',
    path:'/products',
    icon:<HiMiniCube/>,
    lefticon:<HiChevronRight/>
  },
  {
    key:'customers',
    label:'Customers',
    path:'/customers',
    icon: <HiMiniUserCircle/>,
    lefticon:<HiChevronRight/>
  },
  {
    key:'income',
    label:'Income',
    path:'/income',
    icon: <HiMiniScale/>,
    lefticon:<HiChevronRight/>
  },
  {
    key:'promote',
    label:'Promote',
    path:'/promote',
    icon:<HiReceiptPercent/>,
    lefticon:<HiChevronRight/>
  },
  {
    key:'help',
    label:'Help',
    path:'/help',
    icon: <HiChatBubbleLeftEllipsis/>,
    lefticon:<HiChevronRight/>
  }
]