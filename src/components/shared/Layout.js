import React from 'react'
import { Outlet } from 'react-router-dom'
import Sidebar from './Sidebar'
import Navbar from './Navbar'
export default function Layout() {
  return (
    <div className='flex flex-row bg-neutal-100 h-screen w-screen'>
        <Sidebar/>
        <div className='flex-1 overflow-auto'>
        <div>
          <Navbar/>
        </div>
        <div>{<Outlet/>}</div>
        </div>
    </div>
  )
}
