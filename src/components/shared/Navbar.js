import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { FaHandSparkles } from "react-icons/fa6";

export default function Navbar() {
  return (
    <div className="bg-white h-14 px-4 mt-4 mx-4 flex justify-between items-center">
      <div className="flex text-lg font-medium">
        <span> Hello Shahrukh</span>
        <FaHandSparkles className="ml-2 mt-1 text-orange-200"/>
      </div>
      <div className="relative">
        <HiOutlineSearch className="text-gray-400 absolute top-1 left-2" />
        <input
          type="text"
          placeholder="Search"
          className="text-sm border border-white-300 rounded-lg px-4 pl-8"
        ></input>
      </div>
    </div>
  );
}
