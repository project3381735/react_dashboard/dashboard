import React from 'react'

import {
    Bar,
    BarChart,
    ResponsiveContainer,
    XAxis,
    YAxis,
  } from "recharts";
  
  const data = [
    {
      name: "Jan",
      earning: 130,
    },
    {
      name: "Feb",
      earning: 120,
    },
    {
      name: "March",
      earning: 198,
    },
    {
      name: "April",
      earning: 157,
    },
    {
      name: "may",
      earning: 190,
    },
    {
      name: "Jun",
      earning: 100,
    },
    {
      name: "July",
      earning: 190,
    },
    {
      name: "Aug",
      earning: 195,
    },
    {
      name: "Oct",
      earning: 185,
    },
  ];
function DemoChart() {
  return (
    <div className="h-80 bg-white p-4 rounded-lg border border-grey-200 flex flex-col">
        <h1>DemoChart</h1>
         <ResponsiveContainer width={"50%"}>
          <BarChart
            width={500}
            height={500}
            data={data}
            // margin={{ top: 20, left: -10, right: 10, bottom: 0 }}
          >
            <XAxis dataKey={"name"} />
            <YAxis />
            <Bar dataKey={"earning"} fill="#FCE8F3"></Bar>
          </BarChart>
        </ResponsiveContainer>
    </div>
  )
}

export default DemoChart
