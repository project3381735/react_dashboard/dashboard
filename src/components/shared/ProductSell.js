import React, { useState } from "react";
import { HiOutlineSearch } from "react-icons/hi";

const productdata = [
  {
    id: 1,
    title: "iPhone 9",
    description: "An apple mobile which is nothing like apple",
    price: 549,
    total_sell: 30,
    img: "https://i.dummyjson.com/data/products/1/1.jpg",
    stock: "32 in stock",
  },
  {
    id: 2,
    title: "iPhone 9",
    description: "An apple mobile which is nothing like apple",
    price: 549,
    total_sell: 30,
    img: "https://i.dummyjson.com/data/products/1/1.jpg",
    stock: "32 in stock",
  },
  {
    id: 3,
    title: "iPhone 9",
    description: "An apple mobile which is nothing like apple",
    price: 549,
    total_sell: 30,
    img: "https://i.dummyjson.com/data/products/1/1.jpg",
    stock: "32 in stock",
  },
];

function ProductSell() {
  const dummy = [
    { label: "last 30days" },
    { label: "last 60days" },
    { label: "last 90days" },
    { label: "last 6 months" },
  ];
  const [selectoption, setSelectoption] = useState([0]);
  console.log("select option", selectoption);
  return (
    <div className="bg-white px-4 pt-3 mx-2 pb-4 rounded-lg border border-grey-200 my-2">
      <div className="flex flex-row justify-between p-2">
        <div className="flex flex-col">
          <strong className="text-grey-600">ProductSell</strong>
        </div>
        <div className="flex flex-row">
          <div className="relative rounded-sm">
            {" "}
            <HiOutlineSearch className="text-gray-400 absolute top-1 left-2" />
            <input
              type="text"
              placeholder="Search..."
              className="text-sm border border-gray-400 rounded-sm px-4 pl-8"
            ></input>
          </div>
          <div className="w-30 border border-gray-400 rounded-sm mx-2 ">
            <select className="text-gray-400 font-light text-sm" onChange={(e) => setSelectoption(e.target.value)}>
              {dummy.map((item) => (
                <option className="text-gray-400 font-light text-sm">{item.label}</option>
              ))}
            </select>
          </div>
        </div>
      </div>
      <div className="mt-3">
        <table className="justify-center w-full h-full ">
          <thead className="p-8 border border-white-300 px-4 rounded-lg">
            <tr className="text-gray-400 font-light text-sm">
              <td colSpan={6} className="px-4 py-2">
                Product name
              </td>
              <td>Stock</td>
              <td>Price</td>
              <td>Total Sales</td>
            </tr>
          </thead>
          <tbody>
            {productdata.map((products) => (
              <tr key={products.id} rowSpan={2} className="h-14 px-4 py-2">
                <td colSpan={6}>
                  <div className="flex flex-row py-2 px-4">
                    <div className="flex">
                      <img
                        src={products.img} alt={products.img}
                        className="object-cover h-16 w-16 rounded-lg"
                      ></img>
                    </div>
                    <div className="flex flex-col px-2 py-2">
                      <strong className="text-gray-600">
                        {products.title}
                      </strong>
                      <span className="text-gray-400 font-light text-sm">
                        {products.description}
                      </span>
                    </div>
                  </div>
                </td>
                <td className="text-sm">{products.stock}</td>
                <td className="text-sm">
                  <strong className="text-gray-600">$ {products.price}</strong>
                </td>
                <td className="text-sm items-center">{products.total_sell}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default ProductSell;
