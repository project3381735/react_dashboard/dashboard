import React from "react";
import {ResponsiveContainer, Cell, Pie, PieChart, Label } from "recharts";

const data=[
    {
        customer:'female',
        value:25
    },
    {
        customer:'male',
        value:40
    },
    {
        customer:'other',
        value:35
    }
]
function CustomersChart() {
    const COLORS=["#C3DDFD","#E74694",'#9061F9'];


  return (
    <div className="w-[22em] h-[22rem] bg-white p-4 rounded-lg border border-grey-200 flex flex-col ml-4">
      <strong className="text-grey-600">Customers</strong>
      <span className="text-gray-400 font-light text-sm">Customers that buy products</span>
      <div className="w-full mt-3 flex flex-1 text-xs">   
      <ResponsiveContainer width={'100%'} height={'100%'}>
        <PieChart width={600} height={600}>
            <Pie data={data} dataKey={'value'} fill="#8884d8" innerRadius={70} outerRadius={110}>
                {data.map((item,index)=><Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]}/>)}
                <Label className="text-sm text-black font-semibold" position={"center"}>
                  65%new customers
                </Label>
                {/* <Label className="text-sm" value={"Total new customers"} position={"insideBottom"}>Total new customers%
                </Label> */}
            </Pie>
        </PieChart>
      </ResponsiveContainer>
      </div>
    </div>
  );
}

export default CustomersChart;
