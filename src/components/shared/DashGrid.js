import React from "react";
import { IoReaderOutline, IoBagHandleOutline } from "react-icons/io5";
import {
  HiOutlineCurrencyDollar,
  HiMiniScale,
  HiArrowSmallUp,
  HiArrowSmallDown,
} from "react-icons/hi2";

export default function DashGrid() {
  return (
    <div className="flex gap-4 w-full">
      <BoxStyle>
        <div
          className="roundend-full h-24 w-24 flex justify-center items-center bg-green-200"
          style={{ borderRadius: "50%" }}
        >
          <HiOutlineCurrencyDollar className="text-green text-7xl" />
        </div>
        <div className="p-2">
          <span className="text-gray-400 font-light text-sm">Earning</span>
          <div className="flex items-center">
            <strong>$198K</strong>
          </div>
          <div className="text-xs flex">
            <HiArrowSmallUp className="text-green-700 pt-1 font-semibold font-size:2px" />
            <span className="text-green-700 font-semibold">37.8%</span>
            <span className="pl-0.5">this month</span>
          </div>
        </div>
      </BoxStyle>
      <BoxStyle>
        <div
          className="roundend-full h-24 w-24 flex justify-center items-center bg-purple-300"
          style={{ borderRadius: "50%" }}
        >
          <IoReaderOutline className="text-green text-7xl" />
        </div>
        <div className="p-4">
          <span className="text-gray-400 font-light text-sm">Orders</span>
          <div className="flex items-center">
            <strong>$2.4K</strong>
          </div>
          <div className="text-xs flex">
            <HiArrowSmallDown className="text-green-700 pt-1 font-semibold font-size:2px" />
            <span className="text-green-700 font-semibold">2%</span>
            <span className="pl-0.5">this month</span>
          </div>
        </div>
      </BoxStyle>
      <BoxStyle>
        <div
          className="roundend-full h-24 w-24 flex justify-center items-center bg-sky-200"
          style={{ borderRadius: "50%" }}
        >
          <HiMiniScale className="text-green text-7xl" />
        </div>
        <div className="p-4">
          <span className="text-gray-400 font-light text-sm">Balance</span>
          <div className="flex items-center">
            <strong>$2.4K</strong>
          </div>
          <div className="text-xs flex">
            <HiArrowSmallDown className="text-green-700 pt-1 font-semibold font-size:2px" />
            <span className="text-green-700 font-semibold">2%</span>
            <span className="pl-0.5">this month</span>
          </div>
        </div>
      </BoxStyle>
      <BoxStyle>
        <div
          className="roundend-full h-24 w-24 flex justify-center items-center bg-pink-200"
          style={{ borderRadius: "50%" }}
        >
          <IoBagHandleOutline className="text-green text-7xl" />
        </div>
        <div className="p-4">
          <span className="text-gray-400 font-light text-sm">Total Sales</span>
          <div className="flex items-center">
            <strong>$89K</strong>
          </div>
          <div className="text-xs flex">
            <HiArrowSmallUp className="text-green-700 pt-1 font-semibold" />
            <span className="text-green-700 font-semibold">11%</span>
            <span className="pl-0.5">this month</span>
          </div>
        </div>
      </BoxStyle>
    </div>
  );
}

function BoxStyle({ children }) {
  return (
    <div className="bg-white rounded-lg items-center flex flex-1 border border-gray-200 p-4 mx-2 mb-2">
      {children}
    </div>
  );
}
