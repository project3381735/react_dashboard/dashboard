import React from 'react'
import {MdEqualizer} from 'react-icons/md';
import { SIDEBARTOP_LINK } from '../constant/navigation';
import { Link } from 'react-router-dom';
import SlidebarBottom from './SlidebarBottom';
export default function Sidebar() {
  // const menu=[
  //   {
  //     data:[{name:"Roky"},{designation:"project manager"}],
  //     profile:"Profile",
  //     signout:"SignOut"
  //   }
  // ]
  console.log('sidebar')
  return (
    <div className='flex flex-col w-60 p-3 text-white bg-indigo-950'>
      <Link to={"/"}>
        <div className='flex items-center gap-2 px-1 my-4'>
        <MdEqualizer/>
        <span className='text-lg text-indigo-100'>DASHBOARD</span>
        </div>
        </Link>
        <div className='flex-1 flex flex-col py-4'>
          {SIDEBARTOP_LINK.map((item)=>(
              <Link key={item.key} to={item.path} className='flow-root items-center hover:bg-neutral-500 rounded-sm px-3 py-2 font-normal gap-3'>
               <div className='float-left flex gap-3'>
                <span className='text-xl '>{item.icon}</span>
                {item.label}
                </div>
                <span className='float-right'>{item.lefticon}</span>
              </Link>
              
          ))}
        </div>
        <div className='mx-2'>
          <SlidebarBottom/>
          {/* {menu.map((item)=>(
            <Link key={item.id}>
              <span>{item.data[0].name}</span>
              <span>{item.data[0].designation}</span>
            </Link>
          ))} */}
        </div>
    </div>
  )
}
