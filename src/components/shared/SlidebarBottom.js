import React from "react";
import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import { HiArrowDown } from "react-icons/hi2";

export default function SlidebarBottom() {
  return (
    <Menu as="div" className="relative w-full">
      <div>
        <Menu.Button className="flex w-full gap-x-1.5 rounded-md bg-indigo-200 px-1 py-1 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50">
          <div className="flex flex-row items-center px-2">
            <img
              src="https://i.dummyjson.com/data/products/1/1.jpg"
              alt="profile"
              className="roundend-full h-6 w-6 flex items-center"
              style={{ borderRadius: "80%" }}
            ></img>
            <div className="flex flex-col px-2">
              <strong className="text-xs">Evona</strong>
              <span className="text-[0.5rem]">Project Manager</span>
            </div>
            <div className=" ml-12">
              <HiArrowDown
                className="h-5 w-5 text-gray-400 my-1"
                aria-hidden="true"
              />
            </div>
          </div>
        </Menu.Button>
      </div>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-indigo-200 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
          <div className="py-1 px-2">
            <Menu.Item>
              {({ active }) => (
                <a href="/help" className="text-sm text-black">
                  Account settings
                </a>
              )}
            </Menu.Item>

            <Menu.Item>
              {({ active }) => (
                <a href="/signout" className="text-sm text-black">
                  Sign out
                </a>
              )}
            </Menu.Item>
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
