import React, { useState } from "react";
import { Bar, BarChart, ResponsiveContainer, XAxis, } from "recharts";

const data = [
  {
    name: "Jan",
    earning: 130,
  },
  {
    name: "Feb",
    earning: 120,
  },
  {
    name: "March",
    earning: 198,
  },
  {
    name: "April",
    earning: 157,
  },
  {
    name: "may",
    earning: 190,
  },
  {
    name: "Jun",
    earning: 100,
  },
  {
    name: "July",
    earning: 190,
  },
  {
    name: "Aug",
    earning: 195,
  },
  {
    name: "Sept",
    earning: 185,
  },
  {
    name: "Oct",
    earning: 175,
  },
  {
    name: "Nov",
    earning: 170,
  },
  {
    name: "Dec",
    earning: 185,
  },
];

function OverviewChart() {
  const dummyoption=[{label:"Quaterly"},{label:"half-Year"},{label:"Annualy"}];
  const [dummyselect,setDummyselect]=useState([0]);
  console.log("overview monthly select",dummyselect);
  return (
    <div className="h-[22rem] bg-white p-4 w-2/3 rounded-lg border border-grey-200 flex flex-col group-2">
      <div className="flex flex-row justify-between p-2">
        <div className="flex flex-col">
          <strong className="text-grey-600">Overview</strong>
          <span className="text-gray-400 font-light text-sm">
            Monthly Earning
          </span>
        </div>
        <div className="w-30 h-6 border border-gray-400 rounded-sm">
          <select className="text-gray-400 font-light text-sm" onChange={e=>setDummyselect(e.target.value)}>
            {dummyoption.map((item)=>(<option key={item.label}>{item.label}</option>))}
          </select>
        </div>
      </div>
      <div className="w-full mt-3 flex flex-1 text-xs">
        <ResponsiveContainer width={"100%"} height={"100%"}>
          <BarChart
            width={500}
            height={300}
            data={data}
            margin={{ top: 20, left: -10, right: 10, bottom: 0 }}
          >
            <XAxis dataKey={"name"} axisLine={false} />
            {/* <YAxis axisLine={false} /> */}
            <Bar className="rounded-xl" dataKey={"earning"} fill="#E1EFFE"></Bar>
          </BarChart>
        </ResponsiveContainer>
      </div>
    </div>
  );
}

export default OverviewChart;
