import { Route,BrowserRouter,Routes } from "react-router-dom";
import Layout from "./components/shared/Layout";
import Dashboard from "./components/Dashboard";
import Products from "./components/Products";
import Customer from "./components/Customer";
import Signout from "./components/Signout";
import Income from "./components/Income";
import Help from "./components/Help";
import Promote from "./components/Promote";

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Layout/>}>
        <Route index element={<Dashboard/>}/>
        <Route path="/products" element={<Products/>}/>
        <Route path="/customers" element={<Customer/>}/>
        <Route path="/income" element={<Income/>}/>
        <Route path="/promote" element={<Promote/>}/>
        <Route path="/help" element={<Help/>}/>
      </Route>
      <Route path="signout" element={<Signout/>}/>
    </Routes>
    </BrowserRouter>
  );
}

export default App;
